/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import java.util.List;

import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem;
import org.eclipse.osbp.dsl.common.datatypes.IDto;

/**
 * API for a bpm event.
 * <ul>
 * <li>start events</li>
 * <li>gateways</i>
 * <li>end events</i>
 * </ul>
 * <i>Pure</i> events - excluding gateways -
 * <ul>
 * <li>must not perform any actions!</li>
 * <li>must not perform any persisting of dtos in the product persistence</li>
 * </ul>
 */
public abstract class AbstractBPMEvent extends AbstractBlipBPMItem implements IBlipBPMWorkloadModifiableItem {

	/** see {@link org.eclipse.osbp.bpm.api.AbstractBlipBPMItem#AbstractBPMItem(String, String)} */
	protected AbstractBPMEvent(String blipId, String bpmId) {
		super(blipId, bpmId);
	}

	@Override
	@Deprecated
	public final Class<?> getOperativeDtoClass() {
		return null;
	}

	@Override
	@Deprecated
	public final void addOperativeDto(IDto workloadDto, IDto operativeDto) {
	}

	@Override
	@Deprecated
	public final void addOperativeDtos(IDto workloadDto, List<IDto> operativeDtos) {
	}

	@Override
	@Deprecated
	public final void removeAllOperativeDtos(IDto workloadDto) {
	}

	@Override
	@Deprecated
	public final void removeOperativeDto(IDto workloadDto, IDto operativeDto) {
	}

	@Override
	@Deprecated
	public final void removeOperativeDtos(IDto workloadDto, List<IDto> operativeDtos) {
	}

	@Override
	@Deprecated
	public final List<IDto> getOperativeDtos(IDto workloadDto) {
		return null;
	}
}
