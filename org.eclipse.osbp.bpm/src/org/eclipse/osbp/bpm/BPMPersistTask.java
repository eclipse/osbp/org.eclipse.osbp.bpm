/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;


/**
 * API for a bpm system persist task explicitly used to persist workload dtos back into the productive persistence
 */
public class BPMPersistTask extends BPMScriptTask {

	/** see {@link org.eclipse.osbp.bpm.api.BPMScriptTask#AbstractBPMSystemTask(String, String)} */
	protected BPMPersistTask(String blipId, String bpmId, String function) {
		super(blipId, bpmId, function);
	}
}
