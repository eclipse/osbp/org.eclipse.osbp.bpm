/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import java.util.List;

import org.eclipse.osbp.bpm.api.IBlipBPMItem;
import org.eclipse.osbp.dsl.common.datatypes.IDto;


/**
 * API for bpm items. See the <code><a href="../BlipDSL.xtext">blip grammar</a></code>
 * <ul>
 * <li>start events</li>
 * <li>human tasks</li>
 * <li>system tasks</li>
 * <li>persist tasks</li>
 * <li>gateways</li>
 * <li>end events</i>
 * </ul>
 * The API allows modifying the process workload dto.
 */
public abstract class AbstractBlipBPMItem implements IBlipBPMItem {

	/** the process unique id inside the blip */
	private final String fBlipId;
	/** the process unique id inside the BPM definition */
	protected final String fBpmId;

	/**
	 * @param blipId the process unique id inside the blip
	 * @param bpmId the process unique id inside the BPM definition
	 */
	protected AbstractBlipBPMItem(String blipId, String bpmId) {
		fBlipId = blipId;
		fBpmId = bpmId;
	}

	@Override
	abstract public Class<?> getOperativeDtoClass();
	
	@Override
	abstract public List<IDto> getOperativeDtos(IDto workloadDto);

	@Override
	public String getBlipId() {
		return fBlipId;
	}
}
