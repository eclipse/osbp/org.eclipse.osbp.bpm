/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import org.eclipse.osbp.bpm.api.IBlipBPMOutgoing;

/**
 * API for a bpm gateway.
 * <br>
 * Gateways
 * <ul>
 * <li>must not perform any actions!</li>
 * <li>must not perform any persisting of dtos in the product persistence</li>
 * <li>Gateways only have to evaluate the state of the bpm process - depending on the actual workload - , to define which outgoing path should be taken!</li>
 * </ul>
 */
public class BPMSplitGateway extends AbstractBPMEvent {

	public enum GatewayMode {
		/** Split.TYPE_XOR */
		EXCLUSIVE,
		/** Split.TYPE_OR */
		INCLUSIVE,
		/** Split.TYPE_AND */
		PARALLEL,
	}
	
	private final GatewayMode gatewayMode;
	private final IBlipBPMOutgoing[] outgoings;
	
	/** see {@link org.eclipse.osbp.bpm.api.AbstractBPMEvent#AbstractBPMEvent(String, String)} */
	public BPMSplitGateway(String blipId, String bpmId, GatewayMode gatewayMode, IBlipBPMOutgoing... outgoings) {
		super(blipId, bpmId);
		this.gatewayMode = gatewayMode;
		this.outgoings = outgoings;
	}
	
	public GatewayMode getGatewayMode() {
		return gatewayMode;
	}
	
	public IBlipBPMOutgoing[] getOutgoings() {
		return outgoings;
	}
}
