/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import java.util.List;

import org.eclipse.osbp.dsl.common.datatypes.IDto;

/**
 * API for a bpm system task
 */
public class BPMScriptTask extends AbstractBlipBPMItem {

	protected final String function;
	
	/** see {@link org.eclipse.osbp.bpm.api.AbstractBlipBPMItem#AbstractBPMItem(String, String)} */
	public BPMScriptTask(String blipId, String bpmId, String function) {
		super(blipId, bpmId);
		this.function = function;
	}

	public String getFunction() {
		return function;
	}
	
	@Override
	@Deprecated
	public final Class<?> getOperativeDtoClass() {
		return null;
	}

	@Override
	@Deprecated
	public final List<IDto> getOperativeDtos(IDto workloadDto) {
		return null;
	}
}
