/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm;

import java.awt.Button;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.osbp.bpm.api.IBPMServiceTask;
import org.eclipse.osbp.bpm.api.IBlipBPMItem;
import org.eclipse.osbp.bpm.api.IBlipBPMStartInfo;
import org.eclipse.osbp.bpm.api.IBlipBPMUserTask;
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem;
import org.eclipse.osbp.dsl.common.datatypes.IDto;

/**
 * Information for a blip corresponding to a BPM process.
 */
public class BlipBPMStartInfo implements IBlipBPMStartInfo {
	private final String processId;
	private final String name;
	private final String resourceName;
	private final String image;
	private final String iconName;
	private Button button;
	private Integer badge=null;
	private boolean logging=false;
	private Class<BlipBaseFunctionGroup> functionGroupClass;
	/** class defining the workload, has to be an IDto */
	private final Class<IDto> workloadDtoClass;
	/** list of BPM items */
	private final Set<AbstractBlipBPMItem> bpmItems = new HashSet<>();

	/**
	 * @param processId
	 * @param name
	 * @param resourceName
	 * @param imageURL
	 * @param iconName
	 * @param logging
	 * @param roles
	 * @param functionGroupClass
	 * @param workloadDtoClass class defining the workload, has to be an IDto
	 * @param bpmItems list of BPM items
	 */
	public BlipBPMStartInfo(String processId, String name, String resourceName, String imageURL, String iconName, boolean logging, Class<?> functionGroupClass, Class<?> workloadDtoClass, AbstractBlipBPMItem... bpmItems) {
		this.processId = processId;
		this.name = name;
		this.resourceName = resourceName;
		this.image = imageURL;
		this.iconName = iconName;
		setLogging(logging);
		if	((functionGroupClass != null) && BlipBaseFunctionGroup.class.isAssignableFrom(functionGroupClass)) {
			this.functionGroupClass = (Class<BlipBaseFunctionGroup>)functionGroupClass;
		}
		else {
			this.functionGroupClass = null;
		}
		//if	(workloadDtoClass.isInstance(IDto.class)) {
		if	((workloadDtoClass != null) && IDto.class.isAssignableFrom(workloadDtoClass)) {
			this.workloadDtoClass = (Class<IDto>)workloadDtoClass;
		}
		else {
			this.workloadDtoClass = null;
		}
		if	(bpmItems != null) {
			for	(AbstractBlipBPMItem bpmItem : bpmItems) {
				this.bpmItems.add(bpmItem);
			}
		}
	}
	
	@Override
	public boolean isLogging() {
		return logging;
	}
	public void setLogging(boolean logging) {
		this.logging = logging;
	}
	
	public String getProcessId() {
		return processId;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public String getResourceName() {
		return resourceName;
	}
	
	/**
	 * @return the image resource url
	 */
	public String getImage() {
		return image;
	}
	
	public Button getButton() {
		return button;
	}
	public void setButton(Button button) {
		this.button = button;
	}
	public Integer getBadge() {
		return badge;
	}
	public void setBadge(Integer badge) {
		this.badge = badge;
	}
	
	/**
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	@Override
	public String getWorkloadDtoFqn() {
		if	(workloadDtoClass != null) {
			return workloadDtoClass.getCanonicalName();
		}
		return null;
	}
	
	@Override
	public IDto createWorkloadDto() {
		try {
			if	(workloadDtoClass != null) {
				return workloadDtoClass.newInstance();
			}
		}
		catch (InstantiationException | IllegalAccessException e) {
		}
		return null;
	}

	@Override
	public IBlipBPMUserTask getBpmHumanTaskForBlipId(String blipId) {
		IBlipBPMItem bpmItem = getBpmItemForBlipId(blipId);
		if	(bpmItem instanceof AbstractBlipBPMUserTask) {
			return (IBlipBPMUserTask) bpmItem;
		}
		return null;
	}

	@Override
	public IBlipBPMUserTask getBpmHumanTaskForBpmId(String bpmId) {
		IBlipBPMItem bpmItem = getBpmItemForBpmId(bpmId);
		if	(bpmItem instanceof AbstractBlipBPMUserTask) {
			return (IBlipBPMUserTask) bpmItem;
		}
		return null;
	}

	@Override
	public IBlipBPMWorkloadModifiableItem getWorkloadMofifiableBpmItemForBlipId(String blipId) {
		IBlipBPMItem bpmItem = getBpmItemForBlipId(blipId);
		if	(bpmItem instanceof IBlipBPMWorkloadModifiableItem) {
			return (IBlipBPMWorkloadModifiableItem) bpmItem;
		}
		return null;
	}

	@Override
	public IBlipBPMWorkloadModifiableItem getWorkloadMofifiableBpmItemForBpmId(String bpmId) {
		IBlipBPMItem bpmItem = getBpmItemForBpmId(bpmId);
		if	(bpmItem instanceof IBlipBPMWorkloadModifiableItem) {
			return (IBlipBPMWorkloadModifiableItem) bpmItem;
		}
		return null;
	}
	
	@Override
	public IBlipBPMItem getBpmItemForBlipId(String blipId) {
		if	(blipId != null) {
			for	(AbstractBlipBPMItem bpmItem : bpmItems) {
				if	(blipId.equals(bpmItem.getBlipId())) {
					return bpmItem;
				}
			}
		}
		return null;
	}
	
	@Override
	public IBlipBPMItem getBpmItemForBpmId(String bpmId) {
		if	(bpmId != null) {
			for	(AbstractBlipBPMItem bpmItem : bpmItems) {
				if	(bpmId.equals(bpmItem.getBlipId())) {
					return bpmItem;
				}
			}
		}
		return null;
	}

	@Override
	public String getFunctionGroupFqn() {
		if	(functionGroupClass != null) {
			return functionGroupClass.getCanonicalName();
		}
		return null;
	}

	@Override
	public List<IBPMServiceTask> getServiceTasks() {
		List<IBPMServiceTask> serviceTasks = new ArrayList<>();
		for	(AbstractBlipBPMItem bpmItem : bpmItems) {
			if	(bpmItem instanceof IBPMServiceTask) {
				serviceTasks.add((IBPMServiceTask) bpmItem);
			}
		}
		return serviceTasks;
	}
}
